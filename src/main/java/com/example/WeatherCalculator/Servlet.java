package com.example.WeatherCalculator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

                // try catch for error or exception handling.
                // In case a letter is submitted, servlet will advise to enter a number.
                try {
                        PrintWriter out = response.getWriter();
                        response.setContentType("text/html");
                        out.println("<html><head></head><body>");

                        //Parse the string and convert to an integer.
                        int farenheit = Integer.parseInt(request.getParameter("farenheit"));
                        //Calculate the conversion from Farenheit to Celsius
                        int celsius = (farenheit - 32) * 5 / 9;
                        out.println("<h1> Farenheit Conversion to Celsius: </h1>");

                        // Return the calculated value
                        out.println("<p> Your temperature in Celsius is: " + celsius + "</p>");
                        out.println("</body></html>");
                } catch (Exception e) {
                }
        }
}
